package my.mindvalley_umayaeswaran_android_test.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import my.mindvalley_umayaeswaran_android_test.R;
import my.mindvalley_umayaeswaran_android_test_lib.MVFileLoader;


public class GalleryAdapter extends ArrayAdapter {

    Context context;
    ArrayList<String> lstImageURLs = new ArrayList<>();
    int layoutResourceId;

    public GalleryAdapter(Context context, ArrayList<String> lst, int layoutResourceId) {
        super(context, layoutResourceId, lst);
        this.context = context;
        this.lstImageURLs = lst;
        this.layoutResourceId = R.layout.view_gallery_item;
    }

    public int getCount() {
        return lstImageURLs.size();
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imgGridItem = (ImageView) convertView
                    .findViewById(R.id.imgGalleryItem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String currentImageURL = lstImageURLs.get(position);
        new MVFileLoader(context, "temp_1.png", holder.imgGridItem, new MVFileLoader.MVDownloadCommunicator() {
            @Override
            public void fileDownloadSuccess(String filePath) {
            }

            @Override
            public void fileDownloadFailed(String url, String errorMessage) {

            }

            @Override
            public void fileDownloadCancelled(String url) {

            }

            @Override
            public void progressUpdate(String url, int progress) {

            }
        }).download(currentImageURL);
        holder.imgGridItem.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return convertView;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    static class ViewHolder {
        ImageView imgGridItem;
    }
}