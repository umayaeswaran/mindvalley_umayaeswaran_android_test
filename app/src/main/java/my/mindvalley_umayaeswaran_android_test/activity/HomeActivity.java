package my.mindvalley_umayaeswaran_android_test.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import my.mindvalley_umayaeswaran_android_test.R;
import my.mindvalley_umayaeswaran_android_test.fragment.HomeNewFragment;
import my.mindvalley_umayaeswaran_android_test.views.MyProgressDialog;

public class HomeActivity extends AppCompatActivity {

    Context context = this;


    public static void verifyStoragePermissions(Activity activity) {
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    Activity activity = this;

    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private void requestPermission(String permission) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission},
                    101);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
        }
    }


    public MyProgressDialog myProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        if (myProgressDialog == null) {
            myProgressDialog = new MyProgressDialog(context);
            myProgressDialog.setCancelable(false);
        }
        manager = getSupportFragmentManager();
        verifyStoragePermissions(this);
        initView();
    }

    private void initView() {
        HomeNewFragment fragment = new HomeNewFragment();
        changeFragment(fragment, fragment.getClass().toString());
    }

    public FragmentManager manager;

    public void changeFragment(final Fragment fragment, String tag) {
        try {
            String backStateName = fragment.getClass().getName();
            String fragmentTag = backStateName;
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
                FragmentTransaction ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.f_enter, R.anim.f_exit, R.anim.f_pop_enter, R.anim.f_pop_exit);
                ft.replace(R.id.container, fragment, fragmentTag);
                ft.addToBackStack(backStateName);
                ft.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    public void showProgress() {
        try {
            myProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgress() {
        try {
            myProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
