package my.mindvalley_umayaeswaran_android_test.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import my.mindvalley_umayaeswaran_android_test.activity.HomeActivity;
import my.mindvalley_umayaeswaran_android_test.R;
import my.mindvalley_umayaeswaran_android_test.adapter.GalleryAdapter;
import my.mindvalley_umayaeswaran_android_test_lib.MVDataLoader;
import my.mindvalley_umayaeswaran_android_test_lib.connection_utils.NetworkUtils;


public class GalleryFragment extends Fragment {


    public GalleryFragment() {
    }


    Context context;

    HomeActivity homeActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        homeActivity = (HomeActivity) getActivity();
    }

    View rootView;
    GridView gridView;
    SwipeRefreshLayout mSwipeLayout;
    String url = "http://admin.frndzzy.com/webservices_cubix/temp_json_valley.json";
//    String url = "http://pastebin.com/raw/wgkJgazE";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        gridView = (GridView) rootView.findViewById(R.id.gridView);
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (current_max_id < 30) {
                    MVDataLoader.getInstance(getActivity(), new MVDataLoader.MVDownloadCommunicator() {
                        @Override
                        public void dataDownloadSuccess(String filePath) {
                            mSwipeLayout.setRefreshing(false);
                            setUpViews(filePath);
                        }

                        @Override
                        public void dataDownloadFailed(String url, String errorMessage) {
                            homeActivity.hideProgress();
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void dataDownloadCancelled(String url) {
                            homeActivity.hideProgress();
                            Log.d("MINDVALLEY", url + " : CANCELLED");
                        }

                        @Override
                        public void progressUpdate(String url, int progress) {

                        }
                    }, NetworkUtils.CONNECTTION_TYPE_GET).fromURL(url).requestNow();
                } else {
                    Toast.makeText(getActivity(), "Pagination Demo Over", Toast.LENGTH_SHORT).show();
                    mSwipeLayout.setRefreshing(false);
                }
            }
        });
        mSwipeLayout.setColorSchemeResources(R.color.colorPrimaryDark,
                android.R.color.holo_green_light, R.color.colorPrimary,
                android.R.color.holo_red_light);
        setUpViews(null);
        homeActivity.showProgress();
        MVDataLoader.getInstance(getActivity(), new MVDataLoader.MVDownloadCommunicator() {
            @Override
            public void dataDownloadSuccess(String filePath) {
                homeActivity.hideProgress();
                setUpViews(filePath);
            }

            @Override
            public void dataDownloadFailed(String url, String errorMessage) {
                homeActivity.hideProgress();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void dataDownloadCancelled(String url) {
                homeActivity.hideProgress();
                Log.d("MINDVALLEY", url + " : CANCELLED");
            }

            @Override
            public void progressUpdate(String url, int progress) {

            }
        }, NetworkUtils.CONNECTTION_TYPE_GET).fromURL(url).requestNow();
//        new AsyncTaskPost(getActivity(), "", GalleryFragment.this, -1).execute(url);
        return rootView;
    }

    int current_max_id = 0;
    ArrayList<String> lstImages = new ArrayList<>();

    GalleryAdapter adapter;

    private void setUpViews(String data) {
        try {
            final ArrayList<String> lst = getImages(data);
            lstImages.addAll(lst);
            current_max_id = lstImages.size();
            adapter = new GalleryAdapter(getActivity(), lstImages, R.layout.view_gallery_item);
            gridView.setAdapter(adapter);
            int scrollTo = lstImages.size() / 3;
            gridView.scrollTo(scrollTo, 0);
            if (lstImages.size() > 0) {
                rootView.findViewById(R.id.swipe_container).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.txtEmpty).setVisibility(View.GONE);
            } else {
                rootView.findViewById(R.id.swipe_container).setVisibility(View.GONE);
                rootView.findViewById(R.id.txtEmpty).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    String current_selected_key = "regular";

    private ArrayList<String> getImages(String arrayData) {
        ArrayList<String> result = new ArrayList<>();
        try {
            JSONArray jsAData = new JSONArray(arrayData);
            for (int i = 0; i < jsAData.length(); i++) {
                JSONObject tempObject = jsAData.getJSONObject(i);
                result.add(tempObject.getJSONObject("urls").getString(current_selected_key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
