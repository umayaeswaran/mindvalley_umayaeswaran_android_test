package my.mindvalley_umayaeswaran_android_test.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import java.io.File;

import my.mindvalley_umayaeswaran_android_test.R;
import my.mindvalley_umayaeswaran_android_test.activity.HomeActivity;
import my.mindvalley_umayaeswaran_android_test_lib.MVDataLoader;
import my.mindvalley_umayaeswaran_android_test_lib.MVFileLoader;
import my.mindvalley_umayaeswaran_android_test_lib.connection_utils.NetworkUtils;


public class HomeNewFragment extends Fragment {

    public HomeNewFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home_new, container, false);
        setUpViews();
        return rootView;
    }

    HomeActivity homeActivity;

    String excelFile = "https://online.utpb.edu/webapps/dur-browserCheck-bb_bb60/samples/sample.xlsx";
    String pdfFile = "http://www.phoca.cz/demo/phocadownload/phocapdf-demo.pdf";
    String jsonURL = "http://pastebin.com/raw/wgkJgazE";

    private void setUpViews() {
        try {
            homeActivity = (HomeActivity) getActivity();
            rootView.findViewById(R.id.layImageGallery).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GalleryFragment fragment = new GalleryFragment();
                    homeActivity.changeFragment(fragment, fragment.getClass().toString());
                }
            });
            rootView.findViewById(R.id.layExcelFile).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeActivity.showProgress();
                    new MVFileLoader(getActivity(), null, null, new MVFileLoader.MVDownloadCommunicator() {
                        @Override
                        public void fileDownloadSuccess(String filePath) {
                            homeActivity.hideProgress();
                            openFile(filePath);
                        }

                        @Override
                        public void fileDownloadFailed(String url, String errorMessage) {
                            Toast.makeText(getActivity(),errorMessage,Toast.LENGTH_SHORT).show();
                            homeActivity.hideProgress();
                        }

                        @Override
                        public void fileDownloadCancelled(String url) {
                            Log.d("MINDVALLEY", url + " : CANCELLED");
                            homeActivity.hideProgress();
                        }

                        @Override
                        public void progressUpdate(String url, int progress) {

                        }
                    }).download(excelFile);
                }
            });
            rootView.findViewById(R.id.layPDFFile).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeActivity.showProgress();
                    new MVFileLoader(getActivity(), null, null, new MVFileLoader.MVDownloadCommunicator() {
                        @Override
                        public void fileDownloadSuccess(String filePath) {
                            homeActivity.hideProgress();
                            openFile(filePath);
                        }

                        @Override
                        public void fileDownloadFailed(String url, String errorMessage) {
                            Toast.makeText(getActivity(),errorMessage,Toast.LENGTH_SHORT).show();
                            homeActivity.hideProgress();
                        }

                        @Override
                        public void fileDownloadCancelled(String url) {
                            Log.d("MINDVALLEY", url + " : CANCELLED");
                            homeActivity.hideProgress();
                        }

                        @Override
                        public void progressUpdate(String url, int progress) {

                        }
                    }).download(pdfFile);
                }
            });
            rootView.findViewById(R.id.layJSONFromRemote).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeActivity.showProgress();
                    MVDataLoader.getInstance(getActivity(), new MVDataLoader.MVDownloadCommunicator() {
                        @Override
                        public void dataDownloadSuccess(String filePath) {
                            homeActivity.hideProgress();
                            showNotifyDialog(getActivity(), filePath);
                        }

                        @Override
                        public void dataDownloadFailed(String url, String errorMessage) {
                            homeActivity.hideProgress();
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void dataDownloadCancelled(String url) {
                            homeActivity.hideProgress();
                            Log.d("MINDVALLEY", url + " : CANCELLED");
                        }

                        @Override
                        public void progressUpdate(String url, int progress) {

                        }
                    }, NetworkUtils.CONNECTTION_TYPE_GET).fromURL(jsonURL).requestNow();
                }
            });
            rootView.findViewById(R.id.layJSONFromCache).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeActivity.showProgress();
                    MVDataLoader.getInstance(getActivity(), new MVDataLoader.MVDownloadCommunicator() {
                        @Override
                        public void dataDownloadSuccess(String filePath) {
                            homeActivity.hideProgress();
                            showNotifyDialog(getActivity(), filePath);
                        }

                        @Override
                        public void dataDownloadFailed(String url, String errorMessage) {
                            homeActivity.hideProgress();
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void dataDownloadCancelled(String url) {
                            homeActivity.hideProgress();
                            Log.d("MINDVALLEY", url + " : CANCELLED");
                        }

                        @Override
                        public void progressUpdate(String url, int progress) {

                        }
                    }, NetworkUtils.CONNECTTION_TYPE_GET).fromCache(jsonURL).requestNow();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showNotifyDialog(Context context, String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle(context.getString(R.string.app_name) + " Info!");
        alertDialogBuilder.setMessage(msg).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    void openFile(String filePath) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        File exactFile = new File(filePath);
        Uri myUri = Uri.fromFile(exactFile);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext1 = exactFile.getName().substring(
                exactFile.getName().indexOf(".") + 1);
        String type = mime.getMimeTypeFromExtension(ext1);
        intent.setDataAndType(myUri, type);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getActivity().startActivity(intent);
    }

}
